package expandindustria.android.dogsapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import expandindustria.android.dogsapp.data.db.dao.BreedDao
import expandindustria.android.dogsapp.data.db.entity.Breed

@Database(entities = [Breed::class], version = 1, exportSchema = false)
abstract class BreedDatabase: RoomDatabase() {
    abstract fun breedDao(): BreedDao
}