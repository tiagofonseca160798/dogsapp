package expandindustria.android.dogsapp.data.mapper

import expandindustria.android.dogsapp.data.api.model.GetBreedResponse
import expandindustria.android.dogsapp.data.db.entity.Breed

class BreedClassesConverter {
    companion object {
        fun convertApiObjectInDbObject(breedsListFromApi: List<GetBreedResponse>): List<Breed> {
            val breedsList = mutableListOf<Breed>()

            breedsListFromApi.forEach { breedInfo ->
                breedsList.add(
                    Breed(
                        breedInfo.id,
                        breedInfo.name,
                        breedInfo.breedGroup ?: "",
                        breedInfo.origin ?: "",
                        breedInfo.temperament ?: "",
                        if (breedInfo.image != null) breedInfo.image.url else ""
                    )
                )
            }

            return breedsList
        }
    }
}