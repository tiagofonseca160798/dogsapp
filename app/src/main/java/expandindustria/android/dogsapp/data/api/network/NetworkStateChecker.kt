package expandindustria.android.dogsapp.data.api.network

interface NetworkStateChecker {
    fun isNetworkAvailable(): Boolean
}