package expandindustria.android.dogsapp.data.api.network

import retrofit2.Response

/**
 * ErrorHandler class to get the ErrorBody data.
 * This class can also be used to implement responses for each of the HTTP codes.
 */
class ErrorHandler {
    companion object {
        fun getError(response: Response<out Any>): String {
            val errorBody = response.errorBody()?.string()
            return errorBody ?: "Something went wrong"
        }
    }
}