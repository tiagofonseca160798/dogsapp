package expandindustria.android.dogsapp.data.api

import expandindustria.android.dogsapp.data.api.model.GetBreedResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface DogsApiService {
    @GET("breeds")
    suspend fun getBreeds(@Query("page") page: Int, @Query("limit") limit: Int = 25): Response<List<GetBreedResponse>>

    @GET("breeds/search")
    suspend fun getBreedsByName(@Query("q") q: String): Response<List<GetBreedResponse>>
}