package expandindustria.android.dogsapp.data.api.interceptors

import expandindustria.android.dogsapp.data.api.network.NetworkStateChecker
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Interceptor to check if there is connectivity. If not, it throws a NetworkAvailabilityException.
 * In case of there is connectivity, it will proceed to the request.
 */
class NetworkReachabilityInterceptor(private val networkStateChecker: NetworkStateChecker) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!networkStateChecker.isNetworkAvailable()) {
            //throw NetworkAvailabilityException("Network not available")
        }
        val request = chain.request()
        return chain.proceed(request)
    }

}

/**
 * Exception object that will be thrown if there is no connectivity.
 */
class NetworkAvailabilityException(message: String) : Exception(message)