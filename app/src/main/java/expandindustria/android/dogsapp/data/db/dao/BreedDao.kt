package expandindustria.android.dogsapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import expandindustria.android.dogsapp.data.db.entity.Breed
import kotlinx.coroutines.flow.Flow

@Dao
interface BreedDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBreeds(breeds: List<Breed>)

    @Query("SELECT * FROM breed")
    fun getBreeds(): Flow<List<Breed>?>

    @Query("SELECT * FROM breed WHERE name LIKE '%' || :name || '%'")
    fun getBreedsByName(name: String): Flow<List<Breed>?>
}