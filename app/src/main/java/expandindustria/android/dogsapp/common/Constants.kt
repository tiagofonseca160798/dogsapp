package expandindustria.android.dogsapp.common

/**
 * Constants class to hold constant values that are used in diverse places in the project.
 */
class Constants {
    companion object {
        const val LOG_TAG = "DogsApp"
        const val REQUESTS_TIMEOUT = 20
        const val LINEAR_VIEW_TYPE = 0
        const val GRID_VIEW_TYPE = 1
    }
}