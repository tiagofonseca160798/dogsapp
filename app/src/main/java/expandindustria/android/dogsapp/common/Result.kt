package expandindustria.android.dogsapp.common

/**
 * Sealed class to hold three different types of results.
 */
sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Loading(val loading: String) : Result<Nothing>()
    data class Error(val error: String) : Result<Nothing>()
}
