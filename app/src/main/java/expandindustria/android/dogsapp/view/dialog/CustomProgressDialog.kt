package expandindustria.android.dogsapp.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import expandindustria.android.dogsapp.databinding.CustomProgressDialogBinding

class CustomProgressDialog(context: Context): Dialog(context) {
    private lateinit var binding: CustomProgressDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CustomProgressDialogBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)

        setCancelable(false)
        setCanceledOnTouchOutside(false)
    }

    fun setMessage(message: String) {
        binding.loadingMessageTextView.text = message
    }
}