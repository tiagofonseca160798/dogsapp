package expandindustria.android.dogsapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import expandindustria.android.dogsapp.viewmodel.BaseViewModel

abstract class BaseFragment<Binding : ViewBinding, ViewModel : BaseViewModel, ViewModelFactory: ViewModelProvider.Factory> : Fragment() {
    protected var viewModelFactory: ViewModelFactory? = null

    protected lateinit var viewModel: ViewModel
    protected abstract fun getViewModelClass(): Class<ViewModel>

    protected open var binding: Binding? = null
    protected abstract fun getViewBinding(): Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        init()
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupData()
        observeData()
    }

    /**
     * Method to initialize views, like recycler view and the respective adapter.
     */
    abstract fun setupView()

    /**
     * Method to setup the Data into the views. For example, when the data comes from an Intent
     * or from a Bundle, if it comes from another Fragment, you probably want to display that data
     * into some views on Fragment creation.
     */
    abstract fun setupData()

    /**
     * As the name says, this method can be used to initialize the observers to the data, like
     * liveData's or Observables that are in viewModel.
     */
    abstract fun observeData()

    private fun init() {
        binding = getViewBinding()
        if (viewModelFactory != null) {
            viewModel = ViewModelProvider(this, viewModelFactory!!).get(getViewModelClass())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}