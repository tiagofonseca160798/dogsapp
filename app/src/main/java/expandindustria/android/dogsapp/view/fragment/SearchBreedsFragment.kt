package expandindustria.android.dogsapp.view.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.common.Result
import expandindustria.android.dogsapp.databinding.FragmentSearchBreedsBinding
import expandindustria.android.dogsapp.view.BaseActivity
import expandindustria.android.dogsapp.view.BaseFragment
import expandindustria.android.dogsapp.view.adapter.SearchBreedsResultAdapter
import expandindustria.android.dogsapp.viewmodel.viewmodel.SearchBreedsViewModel
import expandindustria.android.dogsapp.viewmodel.factory.SearchBreedsViewModelFactory
import javax.inject.Inject

@AndroidEntryPoint
class SearchBreedsFragment : BaseFragment<FragmentSearchBreedsBinding, SearchBreedsViewModel, SearchBreedsViewModelFactory>() {
    @Inject lateinit var searchBreedsViewModelFactory: SearchBreedsViewModelFactory
    @Inject lateinit var searchBreedsResultAdapter: SearchBreedsResultAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModelFactory = searchBreedsViewModelFactory
        setHasOptionsMenu(true)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
        val searchView = SearchView((activity as BaseActivity).supportActionBar!!.themedContext)
        menu.findItem(R.id.action_search).apply {
            setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
            actionView = searchView
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.searchForBreedsByName(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
    }

    override fun getViewModelClass(): Class<SearchBreedsViewModel> = SearchBreedsViewModel::class.java

    override fun getViewBinding(): FragmentSearchBreedsBinding = FragmentSearchBreedsBinding.inflate(layoutInflater)

    override fun setupView() {
        val layoutManager = LinearLayoutManager(activity)
        binding!!.apply {
            searchBreedsResultRecyclerView.layoutManager = layoutManager
            searchBreedsResultRecyclerView.addItemDecoration(DividerItemDecoration(activity, layoutManager.orientation))
            searchBreedsResultRecyclerView.adapter = searchBreedsResultAdapter
            searchBreedsResultAdapter.setItemClickListener {
                val action = SearchBreedsFragmentDirections.actionNavigationSearchBreedsToNavigationBreedDetails(it)
                findNavController().navigate(action)
            }
        }
    }

    override fun setupData() {
        // no-op
    }

    override fun observeData() {
        viewModel.searchBreedsResultMutableLiveData.observe(viewLifecycleOwner, { searchBreedsResult ->
            (activity as BaseActivity).dismissProgressDialog()

            when (searchBreedsResult) {
                is Result.Loading -> {
                    (activity as BaseActivity).showProgressDialog(searchBreedsResult.loading)
                }
                is Result.Error -> {
                    (activity as BaseActivity).showInfoDialog(searchBreedsResult.error)
                }
                is Result.Success -> {
                    searchBreedsResultAdapter.items.clear()
                    searchBreedsResultAdapter.addItems(searchBreedsResult.data)
                }
            }
        })
    }
}