package expandindustria.android.dogsapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.common.Constants
import expandindustria.android.dogsapp.data.db.entity.Breed
import expandindustria.android.dogsapp.databinding.SearchBreedsResultListItemBinding
import expandindustria.android.dogsapp.view.BaseAdapter


class SearchBreedsResultAdapter: BaseAdapter<Breed>() {
    override fun setViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder {
        return SearchBreedsResultViewHolder(SearchBreedsResultListItemBinding.inflate(LayoutInflater.from(parent!!.context), parent, false))
    }

    override fun onBindData(holder: RecyclerView.ViewHolder?, item: Breed) {
        (holder as SearchBreedsResultViewHolder).bindData(item)
    }

    override fun getViewType(position: Int): Int {
        return Constants.LINEAR_VIEW_TYPE
    }

    inner class SearchBreedsResultViewHolder(private val binding: SearchBreedsResultListItemBinding) : BaseViewHolder(binding.root) {
        override fun <T> bindData(data: T) {
            val breed = data as Breed

            binding.apply {
                breedNameTextView.text = breed.name
                originAndGroupTextView.text = root.context.getString(R.string.origin_category, breed.origin, breed.category)

                root.setOnClickListener {
                    onItemClickListener?.let {
                        it(breed)
                    }
                }
            }
        }
    }
}