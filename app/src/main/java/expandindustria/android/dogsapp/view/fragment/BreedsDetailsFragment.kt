package expandindustria.android.dogsapp.view.fragment

import androidx.navigation.fragment.navArgs
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.databinding.FragmentBreedDetailsBinding
import expandindustria.android.dogsapp.view.BaseFragment
import expandindustria.android.dogsapp.viewmodel.BaseViewModel
import expandindustria.android.dogsapp.viewmodel.factory.BaseViewModelFactory

class BreedsDetailsFragment : BaseFragment<FragmentBreedDetailsBinding, BaseViewModel, BaseViewModelFactory>() {
    override fun getViewModelClass(): Class<BaseViewModel> = BaseViewModel::class.java

    override fun getViewBinding(): FragmentBreedDetailsBinding = FragmentBreedDetailsBinding.inflate(layoutInflater)

    override fun setupView() {
        // no-op
    }

    override fun setupData() {
        val args: BreedsDetailsFragmentArgs by navArgs()
        val selectBreed = args.selectedBreed

        binding!!.apply {
            breedNameTextView.text = selectBreed.name
            breedOriginAndCategoryTextView.text = getString(R.string.origin_category, selectBreed.origin, selectBreed.category)
            breedTemperamentTextView.text = selectBreed.temperament
        }
    }

    override fun observeData() {
        // no-op
    }
}