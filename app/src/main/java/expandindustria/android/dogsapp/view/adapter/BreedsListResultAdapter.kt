package expandindustria.android.dogsapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import expandindustria.android.dogsapp.common.Constants
import expandindustria.android.dogsapp.data.db.entity.Breed
import expandindustria.android.dogsapp.databinding.BreedsListGridItemBinding
import expandindustria.android.dogsapp.databinding.BreedsListListItemBinding
import expandindustria.android.dogsapp.view.BaseAdapter

class BreedsListResultAdapter: BaseAdapter<Breed>() {
    var viewType = Constants.LINEAR_VIEW_TYPE

    override fun setViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder {
        return if (viewType == Constants.LINEAR_VIEW_TYPE)
            BreedListResultViewHolder(BreedsListListItemBinding.inflate(LayoutInflater.from(parent!!.context), parent, false))
        else
            BreedGridResultViewHolder(BreedsListGridItemBinding.inflate(LayoutInflater.from(parent!!.context), parent, false))
    }

    override fun onBindData(holder: RecyclerView.ViewHolder?, item: Breed) {
        if (viewType == Constants.LINEAR_VIEW_TYPE)
            (holder as BreedListResultViewHolder).bindData(item)
        else
            (holder as BreedGridResultViewHolder).bindData(item)
    }

    override fun getViewType(position: Int): Int {
        return Constants.LINEAR_VIEW_TYPE
    }

    fun sort() {
        val newList = items.sortedBy { it.id }
        this.items.clear()
        addItems(newList)
    }

    inner class BreedListResultViewHolder(private val binding: BreedsListListItemBinding) : BaseViewHolder(binding.root) {
        override fun <T> bindData(data: T) {
            val breed = data as Breed

            binding.apply {
                Glide.with(breedImageImageView.context).load(breed.imageUrl).into(breedImageImageView)
                breedNameTextView.text = breed.name

                root.setOnClickListener {
                    onItemClickListener?.let {
                        it(breed)
                    }
                }
            }
        }
    }

    inner class BreedGridResultViewHolder(private val binding: BreedsListGridItemBinding) : BaseViewHolder(binding.root) {
        override fun <T> bindData(data: T) {
            val breed = data as Breed

            binding.apply {
                Glide.with(breedImageImageView.context).load(breed.imageUrl).into(breedImageImageView)
                breedNameTextView.text = breed.name

                root.setOnClickListener {
                    onItemClickListener?.let {
                        it(breed)
                    }
                }
            }
        }
    }
}