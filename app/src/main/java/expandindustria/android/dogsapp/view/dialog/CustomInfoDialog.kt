package expandindustria.android.dogsapp.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import expandindustria.android.dogsapp.databinding.CustomInfoDialogBinding

class CustomInfoDialog(context: Context): Dialog(context) {
    private lateinit var binding: CustomInfoDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CustomInfoDialogBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
    }

    fun setMessage(message: String) {
        binding.errorTextView.text = message
    }
}