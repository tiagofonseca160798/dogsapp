package expandindustria.android.dogsapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import androidx.appcompat.content.res.AppCompatResources
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.common.Constants
import expandindustria.android.dogsapp.common.Result
import expandindustria.android.dogsapp.databinding.FragmentBreedsListBinding
import expandindustria.android.dogsapp.view.BaseActivity
import expandindustria.android.dogsapp.view.BaseFragment
import expandindustria.android.dogsapp.view.adapter.BreedsListResultAdapter
import expandindustria.android.dogsapp.viewmodel.viewmodel.BreedsListViewModel
import expandindustria.android.dogsapp.viewmodel.factory.BreedsListViewModelFactory
import javax.inject.Inject

@AndroidEntryPoint
class BreedsListFragment : BaseFragment<FragmentBreedsListBinding, BreedsListViewModel, BreedsListViewModelFactory>() {
    @Inject lateinit var breedsListViewModelFactory: BreedsListViewModelFactory
    @Inject lateinit var breedsListResultAdapter: BreedsListResultAdapter

    private var page = 0
    private var isScrolling = false

    /*
    This object is used to have more control over when it is necessary to make a new request for a new page.
    Another way to implement this could be using the Paging Library.
     */
    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = binding!!.breedsListRecyclerView.layoutManager as LinearLayoutManager
            val sizeOfTheCurrentList = layoutManager.itemCount
            val visibleItems = layoutManager.childCount
            val topPosition = layoutManager.findFirstVisibleItemPosition()

            val hasReachedToEnd = topPosition + visibleItems >= sizeOfTheCurrentList
            val shouldPaginate = hasReachedToEnd && isScrolling
            if (shouldPaginate) {
                page++
                viewModel.getBreeds(page)
                isScrolling = false
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModelFactory = breedsListViewModelFactory
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getBreeds(0)
    }

    override fun getViewModelClass(): Class<BreedsListViewModel> = BreedsListViewModel::class.java

    override fun getViewBinding(): FragmentBreedsListBinding = FragmentBreedsListBinding.inflate(layoutInflater)

    override fun setupView() {
        breedsListResultAdapter.viewType = Constants.LINEAR_VIEW_TYPE
        binding!!.apply {
            breedsListRecyclerView.layoutManager = getRecyclerViewLayoutManager()
            breedsListRecyclerView.adapter = breedsListResultAdapter
            breedsListRecyclerView.addOnScrollListener(this@BreedsListFragment.onScrollListener)
            breedsListResultAdapter.setItemClickListener {
                val action = BreedsListFragmentDirections.actionNavigationBreedsListToNavigationBreedDetails(it)
                findNavController().navigate(action)
            }

            syncFloatingActionButton.setOnClickListener {
                // Button to be used if the user is opening the application and no data was synchronized before,
                // because he hadn't connectivity. So, if he opens the app and no data is displayed because
                // he keeps without connectivity and he turns on the connection, he can just press this button
                // and data will be displayed.
                viewModel.getBreeds(0)
            }

            changeLayoutFloatingActionButton.setOnClickListener {
                if (breedsListResultAdapter.viewType == Constants.LINEAR_VIEW_TYPE) {
                    breedsListResultAdapter.viewType = Constants.GRID_VIEW_TYPE
                } else {
                    breedsListResultAdapter.viewType = Constants.LINEAR_VIEW_TYPE
                }

                breedsListRecyclerView.layoutManager = getRecyclerViewLayoutManager()
                changeLayoutFloatingActionButton.setImageDrawable(AppCompatResources.getDrawable(this@BreedsListFragment.requireContext(), getFloatingActionButtonDrawable()))
                breedsListRecyclerView.adapter = breedsListResultAdapter
                breedsListResultAdapter.notifyDataSetChanged()
            }

            sortFloatingActionButton.setOnClickListener {
                // The list is reordered if i change it to HashSet instead of mutableSet,
                // but the UI is not updated, even calling notifyDataSetChanged.
                breedsListResultAdapter.sort()
            }
        }
    }

    override fun setupData() {
        // no-op
    }

    override fun observeData() {
        viewModel.breedListResourceMutableLiveData.observe(viewLifecycleOwner, { breedsListResult ->
            (activity as BaseActivity).dismissProgressDialog()

            when (breedsListResult) {
                is Result.Loading -> {
                    (activity as BaseActivity).showProgressDialog(breedsListResult.loading)
                }
                is Result.Error -> {
                    (activity as BaseActivity).showInfoDialog(breedsListResult.error)
                }
                is Result.Success -> {
                    breedsListResultAdapter.addItems(breedsListResult.data)
                }
            }
        })
    }

    private fun getRecyclerViewLayoutManager(): RecyclerView.LayoutManager {
        return if (breedsListResultAdapter.viewType == Constants.GRID_VIEW_TYPE) {
            GridLayoutManager(context, 2)
        } else {
            LinearLayoutManager(context)
        }
    }

    private fun getFloatingActionButtonDrawable(): Int {
        return if (breedsListResultAdapter.viewType == Constants.LINEAR_VIEW_TYPE) {
            R.drawable.ic_baseline_list_24
        } else {
            R.drawable.ic_baseline_grid_on_24
        }
    }
}