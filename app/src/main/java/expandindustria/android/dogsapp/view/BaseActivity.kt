package expandindustria.android.dogsapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.databinding.ActivityBaseBinding
import expandindustria.android.dogsapp.view.dialog.CustomInfoDialog
import expandindustria.android.dogsapp.view.dialog.CustomProgressDialog
import expandindustria.android.dogsapp.viewmodel.BaseViewModel
import expandindustria.android.dogsapp.viewmodel.factory.BaseViewModelFactory
import javax.inject.Inject

@WithFragmentBindings
@AndroidEntryPoint
class BaseActivity : AppCompatActivity() {
    @Inject lateinit var baseViewModelFactory: BaseViewModelFactory
    private var customProgressDialog: CustomProgressDialog? = null
    private var customInfoDialog: CustomInfoDialog? = null
    private lateinit var binding: ActivityBaseBinding
    private lateinit var baseViewModel: BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBaseBinding.inflate(layoutInflater).apply {
            setContentView(root)
            navView.setupWithNavController((supportFragmentManager.findFragmentById(R.id.fragment) as NavHostFragment).navController)
        }

        baseViewModel = ViewModelProvider(this, baseViewModelFactory).get(BaseViewModel::class.java)
    }

    fun showProgressDialog(message: String) {
        dismissProgressDialog()

        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(this)
        }

        customProgressDialog!!.show()
        customProgressDialog!!.setMessage(message)
    }

    fun dismissProgressDialog() {
        if (customProgressDialog != null && customProgressDialog!!.isShowing) {
            customProgressDialog!!.dismiss()
        }
    }

    fun showInfoDialog(message: String) {
        dismissInfoDialog()

        if (customInfoDialog == null) {
            customInfoDialog = CustomInfoDialog(this)
        }

        customInfoDialog!!.show()
        customInfoDialog!!.setMessage(message)
    }

    fun dismissInfoDialog() {
        if (customInfoDialog != null && customInfoDialog!!.isShowing) {
            customInfoDialog!!.dismiss()
        }
    }
}