package expandindustria.android.dogsapp.view

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


abstract class BaseAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    //var items = HashSet<T>()
    var items = mutableSetOf<T>()
    var onItemClickListener: ((T) -> Unit)? = null

    abstract fun setViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder

    abstract fun onBindData(holder: RecyclerView.ViewHolder?, item: T)

    abstract fun getViewType(position: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return setViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBindData(holder, items.elementAt(position))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return getViewType(position)
    }

    fun addItems(items: List<T>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): T {
        return items.elementAt(position)
    }

    abstract class BaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        abstract fun <T> bindData(data: T)
    }

    fun setItemClickListener(listener: (T) -> Unit) {
        onItemClickListener = listener
    }
}