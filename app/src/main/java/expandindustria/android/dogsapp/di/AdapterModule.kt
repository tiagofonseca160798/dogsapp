package expandindustria.android.dogsapp.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import expandindustria.android.dogsapp.view.adapter.BreedsListResultAdapter
import expandindustria.android.dogsapp.view.adapter.SearchBreedsResultAdapter

@Module
@InstallIn(SingletonComponent::class)
class AdapterModule {
    @Provides
    fun provideSearchBreedsResultAdapter(): SearchBreedsResultAdapter {
        return SearchBreedsResultAdapter()
    }

    @Provides
    fun provideBreedsListResultAdapter(): BreedsListResultAdapter {
        return BreedsListResultAdapter()
    }
}