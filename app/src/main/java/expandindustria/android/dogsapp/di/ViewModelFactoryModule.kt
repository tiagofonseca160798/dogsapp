package expandindustria.android.dogsapp.di

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import expandindustria.android.dogsapp.data.api.DogsApiService
import expandindustria.android.dogsapp.data.db.dao.BreedDao
import expandindustria.android.dogsapp.viewmodel.factory.BaseViewModelFactory
import expandindustria.android.dogsapp.viewmodel.factory.BreedsListViewModelFactory
import expandindustria.android.dogsapp.viewmodel.factory.SearchBreedsViewModelFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ViewModelFactoryModule {
    @Singleton
    @Provides
    fun provideBaseViewModelFactory(): BaseViewModelFactory {
        return BaseViewModelFactory()
    }

    @Singleton
    @Provides
    fun provideBreedsListViewModelFactory(
        application: Application,
        dogsApiService: DogsApiService,
        breedDao: BreedDao
    ): BreedsListViewModelFactory {
        return BreedsListViewModelFactory(application, dogsApiService, breedDao)
    }

    @Singleton
    @Provides
    fun provideSearchBreedsViewModelFactory(
        application: Application,
        dogsApiService: DogsApiService,
        breedDao: BreedDao
    ): SearchBreedsViewModelFactory {
        return SearchBreedsViewModelFactory(application, dogsApiService, breedDao)
    }
}