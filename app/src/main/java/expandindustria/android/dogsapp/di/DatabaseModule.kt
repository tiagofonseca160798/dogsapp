package expandindustria.android.dogsapp.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import expandindustria.android.dogsapp.data.db.BreedDatabase
import expandindustria.android.dogsapp.data.db.dao.BreedDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Singleton
    @Provides
    fun provideBreedsDatabase(application: Application): BreedDatabase {
        return Room.databaseBuilder(
            application,
            BreedDatabase::class.java,
            "breeds"
        ).build()
    }

    @Singleton
    @Provides
    fun provideBreedDao(breedDatabase: BreedDatabase): BreedDao {
        return breedDatabase.breedDao()
    }
}