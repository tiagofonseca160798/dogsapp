package expandindustria.android.dogsapp.di

import android.app.Application
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import expandindustria.android.dogsapp.BuildConfig
import expandindustria.android.dogsapp.common.Constants
import expandindustria.android.dogsapp.data.api.DogsApiService
import expandindustria.android.dogsapp.data.api.interceptors.ApiInterceptor
import expandindustria.android.dogsapp.data.api.interceptors.NetworkReachabilityInterceptor
import expandindustria.android.dogsapp.data.api.network.NetworkStateChecker
import expandindustria.android.dogsapp.data.api.network.NetworkStateCheckerImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {
    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .serializeNulls()
            .create()
    }

    @Provides
    @Singleton
    fun providesNetworkStatusChecker(application: Application) : NetworkStateChecker {
        return NetworkStateCheckerImpl(application)
    }

    @Provides
    @Singleton
    fun provideNetworkReachabilityInterceptor(networkStateChecker: NetworkStateChecker): NetworkReachabilityInterceptor {
        return NetworkReachabilityInterceptor(networkStateChecker)
    }

    @Provides
    @Singleton
    fun provideApiInterceptor(): ApiInterceptor {
        return ApiInterceptor()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    @Singleton
    @Provides
    fun provideOkHttpBuilder(networkReachabilityInterceptor: NetworkReachabilityInterceptor, apiInterceptor: ApiInterceptor, httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient
            .Builder()
            .connectTimeout(Constants.REQUESTS_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(Constants.REQUESTS_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .callTimeout(Constants.REQUESTS_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(Constants.REQUESTS_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(networkReachabilityInterceptor)
            .addInterceptor(apiInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideDogsApiService(retrofit: Retrofit): DogsApiService {
        return retrofit.create(DogsApiService::class.java)
    }
}