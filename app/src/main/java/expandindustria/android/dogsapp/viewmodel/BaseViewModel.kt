package expandindustria.android.dogsapp.viewmodel

import androidx.lifecycle.ViewModel

/**
 * BaseViewModel class that can be used to share a method or an object that is used between all ViewModels.
 */
open class BaseViewModel : ViewModel()