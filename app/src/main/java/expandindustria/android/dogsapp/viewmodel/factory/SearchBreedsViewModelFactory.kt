package expandindustria.android.dogsapp.viewmodel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import expandindustria.android.dogsapp.data.api.DogsApiService
import expandindustria.android.dogsapp.data.db.dao.BreedDao
import expandindustria.android.dogsapp.viewmodel.viewmodel.SearchBreedsViewModel

class SearchBreedsViewModelFactory(
    private val application: Application,
    private val dogsApiService: DogsApiService,
    private val breedDao: BreedDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchBreedsViewModel(application, dogsApiService, breedDao) as T
    }
}