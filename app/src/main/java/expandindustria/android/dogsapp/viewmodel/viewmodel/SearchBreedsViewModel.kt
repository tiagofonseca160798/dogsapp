package expandindustria.android.dogsapp.viewmodel.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.common.Result
import expandindustria.android.dogsapp.data.api.DogsApiService
import expandindustria.android.dogsapp.data.api.interceptors.NetworkAvailabilityException
import expandindustria.android.dogsapp.data.db.dao.BreedDao
import expandindustria.android.dogsapp.data.db.entity.Breed
import expandindustria.android.dogsapp.data.mapper.BreedClassesConverter
import expandindustria.android.dogsapp.viewmodel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SearchBreedsViewModel(
    private val application: Application,
    private val dogsApiService: DogsApiService,
    private val breedDao: BreedDao
) : BaseViewModel() {
    var searchBreedsResultMutableLiveData: MutableLiveData<Result<List<Breed>>> = MutableLiveData()

    /**
     * This method is responsible to retrieve information about breeds with a filter that is the name.
     * It will try to get that information from the server, if there is connection with internet and server.
     * In case it can't connect to the server, it will try to retrieve any information from local database also based on the filter.
     *
     * @param breedsName Used to get the breed information from the server or from local database
     */
    fun searchForBreedsByName(breedsName: String?) {
        searchBreedsResultMutableLiveData.value = Result.Loading(application.getString(R.string.loading_result_for_search))

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val getBreedsByNameResponse = dogsApiService.getBreedsByName(breedsName!!)
                if (getBreedsByNameResponse.isSuccessful) {
                    val getBreedByNameResponseBody = getBreedsByNameResponse.body()!!
                    val breedsByNameResult = BreedClassesConverter.convertApiObjectInDbObject(getBreedByNameResponseBody)
                    searchBreedsResultMutableLiveData.postValue(Result.Success(breedsByNameResult))
                } else {
                    searchForBreedByNameFromDb(breedsName)
                }
            } catch (exception: Exception) {
                searchForBreedByNameFromDb(breedsName!!)
            }

            cancel()
        }
    }

    /**
     * Method to retrieve the list of dogs/breeds in the local database for the filter passed
     * If there is no information, it means it was not possible to get the breed at least once or the filter is wrong.
     */
    private fun searchForBreedByNameFromDb(breedsName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            breedDao
                .getBreedsByName(breedsName)
                .collect { breedsListFilteredByName ->
                    if (breedsListFilteredByName != null && breedsListFilteredByName.isNotEmpty()) {
                        searchBreedsResultMutableLiveData.postValue(Result.Success(breedsListFilteredByName))
                    } else {
                        searchBreedsResultMutableLiveData.postValue(Result.Error(application.getString(R.string.no_data)))
                    }
                }
            cancel()
        }
    }
}