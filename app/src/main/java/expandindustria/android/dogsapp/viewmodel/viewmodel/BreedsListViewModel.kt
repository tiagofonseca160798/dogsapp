package expandindustria.android.dogsapp.viewmodel.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import expandindustria.android.dogsapp.R
import expandindustria.android.dogsapp.data.api.DogsApiService
import expandindustria.android.dogsapp.data.db.dao.BreedDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import expandindustria.android.dogsapp.common.Result
import expandindustria.android.dogsapp.data.db.entity.Breed
import expandindustria.android.dogsapp.data.mapper.BreedClassesConverter
import expandindustria.android.dogsapp.viewmodel.BaseViewModel
import kotlinx.coroutines.flow.collect

class BreedsListViewModel(
    private val application: Application,
    private val dogsApiService: DogsApiService,
    private val breedDao: BreedDao
) : BaseViewModel() {
    var breedListResourceMutableLiveData: MutableLiveData<Result<List<Breed>>> = MutableLiveData()

    /**
     * This method is responsible to retrieve information about breeds.
     * It will try to get that information from the server, if there is connection with internet and server.
     * In case it can't connect to the server, it will try to retrieve any information from local database.
     *
     * @param page Used to get the breed information from the server by pages instead of getting everything in one request.
     */
    fun getBreeds(page: Int) {
        breedListResourceMutableLiveData.value = Result.Loading(application.getString(R.string.loading_result))

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val getBreedResponse = dogsApiService.getBreeds(page)
                if (getBreedResponse.isSuccessful) {
                    val getBreedResponseBody = getBreedResponse.body()!!
                    val dogsList = BreedClassesConverter.convertApiObjectInDbObject(getBreedResponseBody)
                    breedDao.insertBreeds(dogsList)
                    breedListResourceMutableLiveData.postValue(Result.Success(dogsList))
                } else {
                    getBreedsFromDb()
                }
            } catch (exception: Exception) {
                getBreedsFromDb()
            }
            cancel()
        }
    }

    /**
     * Method to retrieve the list of breeds in the local database.
     * If there is no information, it means it was not possible to get the breeds at least once.
     */
    private fun getBreedsFromDb() {
        viewModelScope.launch(Dispatchers.IO) {
            breedDao
                .getBreeds()
                .collect { breedListSaved ->
                    if (breedListSaved != null && breedListSaved.isNotEmpty()) {
                        breedListResourceMutableLiveData.postValue(Result.Success(breedListSaved))
                    } else {
                        breedListResourceMutableLiveData.postValue(Result.Error(application.getString(R.string.no_connectivity_and_no_data)))
                    }
                }
            cancel()
        }
    }
}